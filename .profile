echo 'profile...'
## The following snippet from someone's dotfiles that looked elegant
##  doesn't work. It tries to load the entire thing from $HOME/.{..} as one
##  long odd file path rather than split it up. :(
# Load our dotfiles.
#for file in ~/.{aliases,aliases_private,bash_prompt,exports,functions,functions_private}; do
#    echo 'looking for ' $file
#    [ -r "$file" ] && [ -f "$file" ] && source "$file";
#done;
#unset file;
## The following is all I really need anyway:

export PATH=~/.apps/:$PATH

source .aliases
source .aliases_private

setterm --blength 0

source $HOME/.profile_private

source .theme
